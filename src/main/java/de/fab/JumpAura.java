package de.fab;

import de.fab.commands.StartCommand;
import de.fab.events.*;
import de.fab.luckyblocks.block.ALuckyBlock;
import de.fab.luckyblocks.block.BlockManager;
import de.fab.luckyblocks.blocks.HeroBlock;
import de.fab.luckyblocks.blocks.NormalBlock;
import de.fab.luckyblocks.blocks.OpBlock;
import de.fab.luckyblocks.blocks.TitanBlock;
import de.fab.utils.Dictionary;
import de.fab.utils.Manager;
import de.fab.commands.initial.SetSpawn;
import de.fab.config.MainConfig;
import de.fab.events.initial.Join;
import lombok.Getter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Fabian on 07.02.2017.
 */
public class JumpAura extends JavaPlugin
{
    @Getter
    private static JumpAura instance;

    @Getter
    private static MainConfig mainConfig;

    @Getter
    private static BlockManager bm;

    @Getter
    private static Inventory voteInventory;

    @Getter
    private static HashMap<Player, ALuckyBlock> voteList = new HashMap<Player, ALuckyBlock>();

    @Override
    public void onEnable() {
        instance = this;
        mainConfig = new MainConfig(this);
        try {
            mainConfig.init();
            Dictionary.getInstance().loadLanguages();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
        if(Manager.getInstance().getLcfg().isConfigured()){


            Bukkit.getWorlds().add(Manager.getInstance().getLcfg().getLobbySpawn().getLocation().getWorld());
            Bukkit.getWorlds().add(Manager.getInstance().getLcfg().getFightSpawn().getLocation().getWorld());
            Bukkit.getWorlds().add(Manager.getInstance().getLcfg().getGameSpawn().getLocation().getWorld());

            //Register listener
            Bukkit.getPluginManager().registerEvents(new JoinEvent(), this);
            Bukkit.getPluginManager().registerEvents(new DamageEvent(), this);
            Bukkit.getPluginManager().registerEvents(new MoveEvent(), this);
            Bukkit.getPluginManager().registerEvents(new Death_E(), this);
            Bukkit.getPluginManager().registerEvents(new BreakEvent(), this);
            Bukkit.getPluginManager().registerEvents(new QuitEvent(), this);
            Bukkit.getPluginManager().registerEvents(new UseEvent(), this);
            Bukkit.getPluginManager().registerEvents(new PingEvent(), this);
            Bukkit.getPluginManager().registerEvents(new WeatherChangeEvent(), this);
            Bukkit.getPluginManager().registerEvents(new EntityDamageByEntityEvent(), this);
            Bukkit.getPluginManager().registerEvents(new EatEvent(), this);
            Bukkit.getPluginManager().registerEvents(new DropEvent(), this);
            //Configure worlds

            bm = new BlockManager();
            bm.addBlock(new NormalBlock());
            bm.addBlock(new OpBlock());
            bm.addBlock(new HeroBlock());
            bm.addBlock(new TitanBlock());
            voteInventory = Bukkit.createInventory(null, 9, "Vote");
            for(ALuckyBlock b : bm.getBlockslist()){
                ItemStack lucky = new ItemStack(Material.SPONGE);
                ItemMeta lMeta = lucky.getItemMeta();
                lMeta.setDisplayName(b.getName());
                lucky.setItemMeta(lMeta);
                voteInventory.addItem(lucky);
            }
            getCommand("start").setExecutor(new StartCommand());

        } else {
            Bukkit.getPluginManager().registerEvents(new Join(), this);
            getCommand("setspawn").setExecutor(new SetSpawn());
        }

    }

    @Override
    public void onDisable() {
        for(Location location : BreakEvent.destroyedblocks.keySet()){
            location.getWorld().getBlockAt(location.getBlockX(), location.getBlockY(), location.getBlockZ()).setType(BreakEvent.destroyedblocks.get(location));
        }
    }
}
