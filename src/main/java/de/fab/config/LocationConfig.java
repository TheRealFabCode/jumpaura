package de.fab.config;

import de.fab.JumpAura;
import de.fab.config.entries.LocationEntry;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import net.cubespace.Yamler.Config.Config;
import net.cubespace.Yamler.Config.ConfigMode;

import java.io.File;

/**
 * Created by Fabian on 11.02.2017.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode
public final class LocationConfig extends Config {


    private LocationEntry lobbySpawn = new LocationEntry("");

    private LocationEntry gameSpawn = new LocationEntry("");

    private LocationEntry fightSpawn = new LocationEntry("");

    public LocationConfig(JumpAura plugin){


            if (!plugin.getDataFolder().exists() && !plugin.getDataFolder().mkdir())
            {
                plugin.getLogger().severe("Could not load datafolder.");
                return;
            }

            CONFIG_FILE = new File(plugin.getDataFolder(), "locationconfig.yml");
            CONFIG_MODE = ConfigMode.FIELD_IS_KEY;

    }

    public boolean isConfigured()
    {
        return lobbySpawn.isSet() && gameSpawn.isSet() && fightSpawn.isSet();
    }


}


