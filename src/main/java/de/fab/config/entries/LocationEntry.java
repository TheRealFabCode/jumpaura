package de.fab.config.entries;

import lombok.*;
import net.cubespace.Yamler.Config.Config;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * Created by Fabian on 11.02.2017.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class LocationEntry extends Config
{

    private String locationString = "";

    public Location getLocation()
    {
        if(this.locationString.isEmpty())
            return null;

        String[] split = this.locationString.split("::");
        Location loc = new Location(Bukkit.getWorld(split[0]),
                Double.parseDouble(split[1]),
                Double.parseDouble(split[2]),
                Double.parseDouble(split[3]));
        return loc;
    }

    public void setLocation(Location location)
    {
        this.locationString = location.getWorld().getName() + "::" + location.getX() + "::" + location.getY() + "::" + location.getZ();
    }

    public boolean isSet()
    {
        return !locationString.isEmpty();
    }

}
