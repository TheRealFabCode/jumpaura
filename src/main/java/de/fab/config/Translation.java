package de.fab.config;

import de.fab.JumpAura;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.cubespace.Yamler.Config.Config;
import net.cubespace.Yamler.Config.ConfigMode;
import org.bukkit.ChatColor;

import java.io.File;

/**
 * Project: buildingrecoded
 * Package: eu.cc.juyas.building.config
 *
 * @author Drayke
 * @since 31.10.2016.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ToString
@NoArgsConstructor
public final class Translation extends Config
{

    private String language = "Deutsch";

    private String err_unpermitted = "§cDu hast nicht genug Rechte!";
    private String err_languageNotExists = "§cDiese Sprache existiert nicht!";
    private String err_somethingWentWrong = "§cSorry. Es ist etwas schief gegangen!";
    private String err_onlyPlayer = "§cNur für Spieler!";
    private String err_playerNotFound = "§cDer Spieler konnte nicht gefunden werden!";
    private String err_wrongArguments = "§cFalsche Angaben!";
    private String err_commandNotFound = "§cDieser Befehl wurde nicht gefunden.";
    private String err_noNumber = "§cBitte nutzt eine ganzzahlige Nummer!";
    private String write_error = ChatColor.DARK_RED + "Du hast einen Tipp fehler! Bitte überprüfe deine Eingaben!";
    private String spawn_setlobby = ChatColor.GREEN + "Du hast den " + ChatColor.BLUE + "Lobby Spawn " + ChatColor.GREEN + "gesetzt!";
    private String spawn_setgame = ChatColor.GREEN + "Du hast den " + ChatColor.BLUE + "Game Spawn " + ChatColor.GREEN + "gesetzt!";
    private String spawn_setfight = ChatColor.GREEN + "Du hast den " + ChatColor.BLUE + "Fight Spawn " + ChatColor.GREEN + "gesetzt!";
    private String err_spawnset = ChatColor.RED + "Die Spawnpunkte wurden noch nicht gesetzt!";
    private String err_gamecnfg = ChatColor.RED + "Das Spiel wurde noch nicht Konfiguriert Versuche es später erneut!";
    private String game_start_1 = ChatColor.GREEN + "Das Spiel startet in ";
    private String game_start_2 = ChatColor.GREEN + " Sekunden!";
    private String fight_tp_1 = ChatColor.GREEN + "Die Kampfphase beginnt in ";
    private String fight_tp_2 = ChatColor.GREEN + " Sekunden!";
    private String game_starting = ChatColor.GREEN + "Das Spiel startet!";
    private String game_start_stop = ChatColor.RED + "Der Countdown wurde abgebrochen da zu wenig Spieler online sind!";
    private String game_ending = ChatColor.RED + "Der Server startet nun neu!";
    private String nodamage_period = ChatColor.RED + "Die 15 Sekündige Schutzzeit hat begonnen!";
    private String nodamage_period_end = ChatColor.RED + "Die 15 Sekündige Schutzzeit ist nun beendet! Viel Glück!";
    private String game_end_1 = ChatColor.GREEN + "Der Server stoppt in ";
    private String game_end_2 = ChatColor.GREEN + " Sekunden!";
    private String win = ChatColor.GOLD + " hat das Spiel gewonnen!";
    private String kill_1 = ChatColor.BLUE + " hat ";
    private String kill_2 = ChatColor.BLUE + " getötet!";
    private String kill_3 = ChatColor.BLUE + " ist gestorben!";
    
    /**
     * Instantiates a new Translation.
     *
     * @param plugin   the plugin
     * @param language the language
     */
    public Translation(JumpAura plugin, String language)
    {
        if (!new File(plugin.getDataFolder().getAbsolutePath() + "/lang").exists() &&
                !new File(plugin.getDataFolder().getAbsolutePath() + "/lang").mkdir())
        {
            plugin.getLogger().severe("Could not load language folder.");
            return;
        }

        CONFIG_FILE = new File(plugin.getDataFolder().getAbsolutePath() + "/lang", language);
        CONFIG_MODE = ConfigMode.FIELD_IS_KEY;
    }

}