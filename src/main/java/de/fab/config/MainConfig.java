package de.fab.config;

import de.fab.JumpAura;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import net.cubespace.Yamler.Config.Comment;
import net.cubespace.Yamler.Config.Config;
import net.cubespace.Yamler.Config.ConfigMode;
import org.bukkit.ChatColor;

import java.io.File;
import lombok.Getter;

/**
 * <h1>MMO-Events</h1>
 * The Main configuration for the
 * EventMain.
 *
 * @author Juyas, Drayke
 * @version 1.0
 * @since 05.02.2017
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode
public final class MainConfig extends Config
{

    @Comment("The default language file")
    private String defaultLanguage = "de";

    @Comment("The chat prefix used for this plugin.")
    private String prefix = ChatColor.WHITE + "[" + ChatColor.GREEN + "Jump" + ChatColor.GRAY + "Aura" + ChatColor.WHITE + "] " + ChatColor.AQUA;

    @Comment("Max Player value")
    private int max_players = 8;
    
    @Comment("Min Player to Start!")
    private int min_player_start = 2;

    private boolean debug = false;




    /**
     * Instantiates a new Config
     *
     * @param plugin the plugin
     */
    public MainConfig(JumpAura plugin)
    {

        if (!plugin.getDataFolder().exists() && !plugin.getDataFolder().mkdir())
        {
            plugin.getLogger().severe("Could not load datafolder.");
            return;
        }

        CONFIG_FILE = new File(plugin.getDataFolder(), "config.yml");
        CONFIG_MODE = ConfigMode.FIELD_IS_KEY;

    }

}
