package de.fab.events.initial;

import de.fab.utils.GeneralCore;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Fabian on 11.02.2017.
 */
public class Join extends GeneralCore implements Listener{
    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        Player p = e.getPlayer();
        if(p.hasPermission("games.admin")){
            message(p, defaultTranslation().getErr_spawnset());
        } else {
            p.kickPlayer(defaultTranslation().getErr_gamecnfg());
        }
        
    }
}
