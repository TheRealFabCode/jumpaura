package de.fab.events;

import de.fab.JumpAura;
import de.fab.luckyblocks.blocks.TitanBlock;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class EntityDamageByEntityEvent implements Listener {

    @EventHandler
    public void onDamage(org.bukkit.event.entity.EntityDamageByEntityEvent e) {
        if (JumpAura.getBm().getBlock() instanceof TitanBlock && e.getDamager() instanceof Player && e.getEntity() instanceof Player) {
            Player damaged = (Player) e.getEntity();
            ItemStack itemStack = ((Player) e.getDamager()).getItemInHand();
            if (itemStack != null) {
                TitanBlock.EffectSword effectSword = getEffect(itemStack);
                switch (effectSword) {
                    case BLINDNESS:
                        damaged.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * 2, 0));
                        break;
                    case POISON:
                        damaged.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20 * 2, 0));
                        break;
                    case WITHER:
                        damaged.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 20 * 2, 0));
                        break;
                    case NONE:
                        break;
                    case POISON2:
                        damaged.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20 * 2, 1));
                        break;
                    case WITHER2:
                        damaged.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 20 * 2, 1));
                        break;
                    case SLOWNESS:
                        damaged.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 2, 0));
                        break;
                    case WEAKNESS:
                        damaged.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 20 * 2, 0));
                        break;
                    case SLOWNESS2:
                        damaged.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 2, 1));
                        break;
                    case WEAKNESS2:
                        damaged.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 20 * 2, 1));
                        break;
                    case BLINDNESS2:
                        damaged.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * 2, 1));
                        break;
                }
                if(itemStack.getType() == Material.IRON_SWORD){
                    damaged.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20 * 3, 0));
                }
                if(itemStack.getType() == Material.STONE_SWORD){
                    damaged.setFireTicks(20 * 2);
                }
            }
        }
    }


    public TitanBlock.EffectSword getEffect(ItemStack itemStack) {
        TitanBlock.EffectSword effectSword = TitanBlock.EffectSword.NONE;

        String name = itemStack.getItemMeta().getDisplayName();
        try {
            String split[] = name.split("-");
            String effect = split[1];
            if (!effect.isEmpty()) {
                effect = effect.replaceAll(" ", "");
                for (TitanBlock.EffectSword effectSword1 : TitanBlock.EffectSword.values()) {
                    if (effectSword1.name().equalsIgnoreCase(effect)) {
                        effectSword = effectSword1;
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return effectSword;
    }

}
