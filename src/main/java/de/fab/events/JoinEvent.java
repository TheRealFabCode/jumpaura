package de.fab.events;

import de.fab.enumList.GameStats;
import de.fab.JumpAura;
import de.fab.utils.GeneralCore;
import de.fab.utils.Manager;
import de.fab.utils.Timer;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Fabian on 11.02.2017.
 */
public class JoinEvent extends GeneralCore implements Listener{
    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        Player p = e.getPlayer();
        e.setJoinMessage(null);
        p.getInventory().clear();
        p.setMaxHealth(20);

        for(PotionEffect pe : p.getActivePotionEffects()){
            p.removePotionEffect(pe.getType());
        }

        new BukkitRunnable() {

            @Override
            public void run() {
                if(Manager.getInstance().isStat(GameStats.LOBBY)){
                    Manager.getInstance().addPlayer(p);
                }

                if(Manager.getInstance().isStat(GameStats.INGAME)){
                    Manager.getInstance().addSpec(p);
                    p.setGameMode(GameMode.SPECTATOR);
                    p.teleport(Manager.getInstance().getLcfg().getGameSpawn().getLocation());
                }

                if(Manager.getInstance().isStat(GameStats.END)){
                    Manager.getInstance().addSpec(p);
                    p.setGameMode(GameMode.SPECTATOR);
                    p.teleport(Manager.getInstance().getLcfg().getFightSpawn().getLocation());
                }

                if(Manager.getInstance().getPlayers().contains(p) && Manager.getInstance().isStat(GameStats.LOBBY)){
                    p.setGameMode(GameMode.ADVENTURE);
                    p.teleport(Manager.getInstance().getLcfg().getLobbySpawn().getLocation());
                    for(Player pl : Manager.getInstance().getPlayers()){
                        message(pl, ChatColor.BLUE + p.getDisplayName() + ChatColor.GREEN + " hat das Spiel betreten! " + ChatColor.BLUE + Manager.getInstance().getPlayers().size() + "/" + JumpAura.getMainConfig().getMax_players());
                    }

                    ItemStack voteItem = new ItemStack(Material.BLAZE_POWDER);
                    ItemMeta voteMeta = voteItem.getItemMeta();
                    voteMeta.setDisplayName("§6Vote for Lucky Block");
                    voteItem.setItemMeta(voteMeta);
                    p.getInventory().addItem(voteItem);
                    if(Manager.getInstance().getPlayers().size() >= JumpAura.getMainConfig().getMin_player_start() && !Manager.getInstance().getTimer().started()){
                        Timer t = Manager.getInstance().getTimer();
                        t.Startup();
                        t.start();
                    }
                    Manager.getInstance().getLcfg().getLobbySpawn().getLocation().getWorld().setDifficulty(Difficulty.PEACEFUL);
                    Manager.getInstance().getLcfg().getGameSpawn().getLocation().getWorld().setDifficulty(Difficulty.PEACEFUL);
                    Manager.getInstance().getLcfg().getFightSpawn().getLocation().getWorld().setDifficulty(Difficulty.PEACEFUL);
                    Manager.getInstance().getLcfg().getLobbySpawn().getLocation().getWorld().setGameRuleValue("doDaylightCycle", "false");
                    Manager.getInstance().getLcfg().getGameSpawn().getLocation().getWorld().setGameRuleValue("doDaylightCycle", "false");
                    Manager.getInstance().getLcfg().getFightSpawn().getLocation().getWorld().setGameRuleValue("doDaylightCycle", "false");
                    Manager.getInstance().getLcfg().getLobbySpawn().getLocation().getWorld().setGameRuleValue("doMobSpawning", "false");
                    Manager.getInstance().getLcfg().getGameSpawn().getLocation().getWorld().setGameRuleValue("doMobSpawning", "false");
                    Manager.getInstance().getLcfg().getFightSpawn().getLocation().getWorld().setGameRuleValue("doMobSpawning", "false");
                }
            }
        }.runTaskLater(JumpAura.getInstance(), 5);


    }
}
