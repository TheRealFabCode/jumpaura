package de.fab.events;

import java.util.HashMap;
import java.util.Map;

import de.fab.JumpAura;
import de.fab.utils.GeneralCore;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BreakEvent extends GeneralCore implements Listener{

    public static Map<Location, Material> destroyedblocks = new HashMap<Location, Material>();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBreak(BlockBreakEvent e) {
        Block b = e.getBlock();
        if (b.getType() == Material.SPONGE) {
            destroyedblocks.put(b.getLocation(), b.getType());
            JumpAura.getBm().getBlock().onBreak(e.getPlayer());
            e.setCancelled(true);
            b.setType(Material.AIR);
        } else  {
            e.setCancelled(true);
        }

    }
}
