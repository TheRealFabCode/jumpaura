package de.fab.events;

import de.fab.JumpAura;
import de.fab.enumList.GameStats;
import de.fab.luckyblocks.block.ALuckyBlock;
import de.fab.utils.Manager;
import de.fab.utils.Timer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

public class UseEvent implements Listener {

    @EventHandler
    public void onItemUse(PlayerInteractEvent event){
        Timer t = Manager.getInstance().getTimer();
        if (Manager.getInstance().isStat(GameStats.LOBBY)) {
            if (t.started()) {
                if(event.hasItem() && event.getItem().getType() == Material.BLAZE_POWDER){
                    event.getPlayer().openInventory(JumpAura.getVoteInventory());
                }
            }
            event.setCancelled(true);
        }
        if ((event.getAction() == Action.LEFT_CLICK_AIR) || (event.getAction() == Action.LEFT_CLICK_BLOCK))
        {
            ItemStack itemStack = event.getItem();
            if(itemStack != null){
                String name = itemStack.getItemMeta().getDisplayName();
                try{
                    String split[] = name.split("-");
                    String dName = split[0];
                    dName = dName.replaceAll(" ", "");
                    if(dName.equalsIgnoreCase("§9TitanGreatsword") || dName.equalsIgnoreCase("§aCitrinGreatsword") || dName.equalsIgnoreCase("§4NetherGreatsword")){
                        Player p = event.getPlayer();
                        p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20 * 2, 10));
                    }
                } catch(Exception ex) {

                }
            }
        }


    }

    @EventHandler
    public void onInventoryClickItem(InventoryClickEvent event) {
        if(event.getInventory().getName().equalsIgnoreCase("Vote")){
            event.setCancelled(true);
            if(event.getCurrentItem() != null){
                ItemStack item = event.getCurrentItem();
                if(item.hasItemMeta() && item.getItemMeta().getDisplayName() != null && JumpAura.getBm().getLuckyBlockByName(item.getItemMeta().getDisplayName()) != null){
                    Player p = (Player) event.getWhoClicked();
                    ALuckyBlock luckyBlock = JumpAura.getBm().getLuckyBlockByName(item.getItemMeta().getDisplayName());
                    if(JumpAura.getVoteList().containsKey(p)){
                        ALuckyBlock oldVote = JumpAura.getVoteList().get(p);
                        JumpAura.getVoteList().remove(p);

                        int ovotes = 0;
                        for(Player p2 : JumpAura.getVoteList().keySet()){
                            ALuckyBlock b = JumpAura.getVoteList().get(p2);
                            if(b == oldVote){
                                ovotes++;
                            }
                        }

                        ItemStack oldItem = JumpAura.getVoteInventory().getItem(JumpAura.getBm().getBlockslist().indexOf(oldVote));

                        ItemMeta oldMeta = oldItem.getItemMeta();
                        oldMeta.setLore(null);
                        List<String> odescription = new ArrayList<String>();
                        odescription.add("Votes: " + ovotes);
                        oldMeta.setLore(odescription);
                        oldItem.setItemMeta(oldMeta);
                        JumpAura.getVoteInventory().setItem(JumpAura.getBm().getBlockslist().indexOf(oldVote), oldItem);

                        JumpAura.getVoteList().put(p, luckyBlock);

                        ItemMeta meta = item.getItemMeta();
                        List<String> description = new ArrayList<String>();
                        int votes = 0;
                        for(Player p2 : JumpAura.getVoteList().keySet()){
                            ALuckyBlock b = JumpAura.getVoteList().get(p2);
                            if(b == luckyBlock){
                                votes++;
                            }
                        }
                        description.add("Votes: " + votes);
                        meta.setLore(description);
                        item.setItemMeta(meta);
                        JumpAura.getVoteInventory().setItem(JumpAura.getBm().getBlockslist().indexOf(luckyBlock), item);
                    } else {
                        JumpAura.getVoteList().put(p, luckyBlock);
                        JumpAura.getVoteInventory().remove(item);
                        ItemMeta meta = item.getItemMeta();
                        List<String> description = new ArrayList<String>();
                        int votes = 0;
                        for(Player p2 : JumpAura.getVoteList().keySet()){
                            ALuckyBlock b = JumpAura.getVoteList().get(p2);
                            if(b == luckyBlock){
                                votes++;
                            }
                        }
                        description.add("Votes: " + votes);
                        meta.setLore(description);
                        item.setItemMeta(meta);
                        JumpAura.getVoteInventory().setItem(JumpAura.getBm().getBlockslist().indexOf(luckyBlock), item);
                    }
                }
            }
        }
    }



}
