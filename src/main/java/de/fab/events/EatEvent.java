package de.fab.events;

import de.fab.JumpAura;
import de.fab.luckyblocks.blocks.TitanBlock;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class EatEvent implements Listener {

    @EventHandler
    public void eatEvent(PlayerItemConsumeEvent event){
        Player p = (Player) event.getPlayer();
        ItemStack itemStack = event.getItem();
        if(JumpAura.getBm().getBlock() instanceof TitanBlock){
            if(itemStack != null && itemStack.getItemMeta().getDisplayName() != null && itemStack.getItemMeta().getDisplayName().equalsIgnoreCase("§bDiamond Apple")){
                event.setCancelled(true);
                ItemStack inHand = p.getItemInHand();
                inHand.setAmount(inHand.getAmount() - 1);
                p.setItemInHand(inHand);
                p.setFoodLevel(p.getFoodLevel() + 3);
                p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 5, 2));
                p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 20 * 60 * 2, 1));
            }
        }
    }

}
