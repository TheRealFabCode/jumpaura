/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fab.events;

import de.fab.JumpAura;
import de.fab.enumList.GameStats;
import de.fab.enumList.ItemRarity;
import de.fab.utils.GeneralCore;
import de.fab.utils.Manager;
import de.fab.utils.Timer;
import javafx.scene.paint.Color;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import java.util.Random;

/**
 *
 * @author Fab
 */
public class MoveEvent extends GeneralCore implements Listener{
    
    @EventHandler
    public void onMove(PlayerMoveEvent e){
        if(Manager.getInstance().isStat(GameStats.INGAME)){
            if(e.getTo().getY() == 0){
                e.getPlayer().teleport(Manager.getInstance().getLcfg().getGameSpawn().getLocation());
            }
        }
        if(Manager.getInstance().isStat(GameStats.INGAME)){
            Timer t = Manager.getInstance().getTimer();
            Location loc = new Location(e.getPlayer().getWorld(), e.getPlayer().getLocation().getX(), e.getPlayer().getLocation().getY(), e.getPlayer().getLocation().getZ());

            if(loc.getBlock().getType() == Material.GOLD_PLATE && !t.started()){
                t.End();
                e.getPlayer().getInventory().addItem(JumpAura.getBm().getBlock().getLootByRarity(ItemRarity.ULTRARARE).get(new Random().nextInt(JumpAura.getBm().getBlock().getLootByRarity(ItemRarity.ULTRARARE).size())).item);
                for(Player p : Bukkit.getOnlinePlayers()){
                    message(p, e.getPlayer().getName() + ChatColor.GREEN + " hat die Jump Phase gewonnen!");
                }
                t.start();
            }
        }
    }
    
}
