package de.fab.events;

import de.fab.JumpAura;
import de.fab.utils.Manager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import java.util.HashMap;
import java.util.Map;

public class PingEvent implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPing(ServerListPingEvent e){

        switch (Manager.getInstance().getCurrentStat()){
            case LOBBY:
                e.setMotd("§a[Lobby]");
                break;
            case INGAME:
                e.setMotd("§e[Ingame]");
                break;
            case END:
                e.setMotd("§c[Stopping]");
                break;
        }
        e.setMaxPlayers(JumpAura.getMainConfig().getMax_players());
    }

}
