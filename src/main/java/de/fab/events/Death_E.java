package de.fab.events;

import de.fab.enumList.GameStats;
import de.fab.utils.GeneralCore;
import de.fab.utils.Manager;
import de.fab.utils.Timer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class Death_E extends GeneralCore implements Listener{
    
    @EventHandler
    public void onDeath(PlayerDeathEvent e){
        if(Manager.getInstance().isStat(GameStats.END)){
            Manager.getInstance().getPlayers().remove(e.getEntity());
            Manager.getInstance().getSpectators().add(e.getEntity());
            Player p = e.getEntity();
            p.setGameMode(GameMode.SPECTATOR);
            if(p.getKiller() != null) {
                e.setDeathMessage(null);
                for(Player pl : Bukkit.getOnlinePlayers()){
                    message(pl, ChatColor.GREEN + p.getKiller().getDisplayName() + defaultTranslation().getKill_1() + ChatColor.RED + p.getDisplayName() + defaultTranslation().getKill_2());
                }

            } else {
                e.setDeathMessage(null);
                for(Player pl : Bukkit.getOnlinePlayers()){
                    message(pl, ChatColor.RED + p.getDisplayName() + defaultTranslation().getKill_3());
                }
            }



            if(Manager.getInstance().getPlayers().size() < 2){
                Player winner = Manager.getInstance().getPlayers().get(0);
                for(Player player : Bukkit.getOnlinePlayers()){
                    message(player, ChatColor.GREEN + winner.getDisplayName() + defaultTranslation().getWin() + " ");
                }
                Timer t = Manager.getInstance().getTimer();
                t.End();
                t.start();
            }
        }
    }
}
