/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fab.events;

import de.fab.enumList.GameStats;
import de.fab.JumpAura;
import de.fab.utils.GeneralCore;
import de.fab.utils.Manager;
import de.fab.utils.Timer;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 *
 * @author Fab
 */
public class QuitEvent extends GeneralCore implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Timer t = Manager.getInstance().getTimer();
        e.setQuitMessage(null);
        if(Manager.getInstance().getPlayers().contains(e.getPlayer())){
            Manager.getInstance().removePlayer(e.getPlayer());
        }

        for(Player pl : Manager.getInstance().getPlayers()){
            message(pl, ChatColor.BLUE + e.getPlayer().getDisplayName() + ChatColor.GREEN + " hat das Spiel verlassen! " + ChatColor.BLUE + Manager.getInstance().getPlayers().size() + "/" + JumpAura.getMainConfig().getMax_players());
        }


        if (Manager.getInstance().isStat(GameStats.LOBBY)) {
            if (t.started()) {
                if (Manager.getInstance().getPlayers().contains(e.getPlayer()) && Manager.getInstance().getPlayers().size() < JumpAura.getMainConfig().getMin_player_start()) {
                    t.stop();
                    t.Startup();
                }
            }
        }
        if (Manager.getInstance().isStat(GameStats.INGAME)) {
            if (Manager.getInstance().getPlayers().contains(e.getPlayer()) && Manager.getInstance().getPlayers().size() < 2) {
                Manager.getInstance().setCurrentStat(GameStats.END);
                t.End();
                t.start();
            }
        }
        if (Manager.getInstance().isStat(GameStats.END)) {
            if (Manager.getInstance().getPlayers().contains(e.getPlayer()) && Manager.getInstance().getPlayers().size() < 2 && !t.started()) {
                Manager.getInstance().setCurrentStat(GameStats.END);
                t.End();
                t.start();
            }
        }
    }

}
