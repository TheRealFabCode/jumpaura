package de.fab.events;

import de.fab.JumpAura;
import de.fab.enumList.GameStats;
import de.fab.luckyblocks.blocks.TitanBlock;
import de.fab.utils.Manager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

public class DamageEvent implements Listener {
    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (Manager.getInstance().isStat(GameStats.LOBBY)) {
            e.setCancelled(true);
        }
        if (Manager.getInstance().isStat(GameStats.INGAME)) {
            e.setCancelled(true);
        }
        if (Manager.getInstance().isStat(GameStats.END)) {
            if (!Manager.getInstance().isDamage()) {
                e.setCancelled(true);
            }
        }
        if (JumpAura.getBm().getBlock() instanceof TitanBlock) {
            if (e.getEntity() instanceof Player) {
                Player p = (Player) e.getEntity();
                if (p.getInventory().getBoots() != null && p.getInventory().getHelmet() != null && p.getInventory().getChestplate() != null && p.getInventory().getLeggings() != null && p.getInventory().getBoots().getItemMeta().getDisplayName() != null && p.getInventory().getHelmet().getItemMeta().getDisplayName() != null && p.getInventory().getChestplate().getItemMeta().getDisplayName() != null && p.getInventory().getLeggings().getItemMeta().getDisplayName() != null) {
                    if (p.getInventory().getBoots().getItemMeta().getDisplayName().equalsIgnoreCase("§4Nether Boots") && p.getInventory().getLeggings().getItemMeta().getDisplayName().equalsIgnoreCase("§4Nether Leggings") && p.getInventory().getChestplate().getItemMeta().getDisplayName().equalsIgnoreCase("§4Nether Chestplate") && p.getInventory().getHelmet().getItemMeta().getDisplayName().equalsIgnoreCase("§4Nether Helmet")) {
                        e.setCancelled(e.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK);
                        e.setCancelled(e.getCause() == EntityDamageEvent.DamageCause.FIRE);
                    }
                }
            }

        }


    }
}
