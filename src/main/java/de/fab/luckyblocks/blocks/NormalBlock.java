/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fab.luckyblocks.blocks;

import de.fab.enumList.ItemRarity;
import de.fab.luckyblocks.block.AExecute;
import de.fab.luckyblocks.block.ALuckyBlock;
import de.fab.luckyblocks.block.Loot;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author Fabian
 */
public class NormalBlock extends ALuckyBlock {

    public NormalBlock() {
        super("Lucky Block", "https://filebin.net/bi9zs582xos19ody/LuckyPack__1_.zip?t=6283cfuf");
    }

    @Override
    public void init() {
        addLoot(new Loot(new ItemStack(Material.WOOD_SWORD), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.STONE_SWORD), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.IRON_SWORD), this, ItemRarity.ULTRARARE));
        addLoot(new Loot(new ItemStack(Material.IRON_CHESTPLATE), this, ItemRarity.ULTRARARE));
        addLoot(new Loot(new ItemStack(Material.IRON_BOOTS), this, ItemRarity.ULTRARARE));
        addLoot(new Loot(new ItemStack(Material.GOLD_LEGGINGS), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.COOKED_BEEF, 2), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.APPLE, 2), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.GOLDEN_APPLE), this, ItemRarity.ULTRARARE));
        addLoot(new Loot(new ItemStack(Material.BOW), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.ARROW, 6), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.GOLD_CHESTPLATE), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.GOLD_BOOTS), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.LEATHER_BOOTS), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.LEATHER_CHESTPLATE), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.LEATHER_HELMET), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.LEATHER_LEGGINGS), this, ItemRarity.USUALLY));

        ItemStack gh5 = new ItemStack(Material.BOW);
        gh5.addEnchantment(Enchantment.ARROW_DAMAGE, 2);
        ItemMeta gh15 = gh5.getItemMeta();
        gh15.setDisplayName("§5GODZILLA BOW");
        gh15.addEnchant(Enchantment.ARROW_DAMAGE, 2, true);
        gh5.setItemMeta(gh15);

        addLoot(new Loot(gh5, this, ItemRarity.ULTRARARE));

        ItemStack gh = new ItemStack(Material.GOLD_HELMET);
        gh.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        ItemMeta gh1 = gh.getItemMeta();
        gh1.setDisplayName("§5GODZILLA HELMET");
        gh1.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
        gh.setItemMeta(gh1);

        addLoot(new Loot(gh, this, ItemRarity.RARE));

        ItemStack gl = new ItemStack(Material.GOLD_LEGGINGS);
        ItemMeta gl1 = gl.getItemMeta();
        gl1.setDisplayName("§5GODZILLA LEGGINS");
        gl1.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
        gl.setItemMeta(gl1);

        addLoot(new Loot(gl, this, ItemRarity.RARE));

        ItemStack gb1 = new ItemStack(Material.GOLD_BOOTS);
        ItemMeta gbm1 = gb1.getItemMeta();
        gbm1.setDisplayName("§5GODZILLA BOOTS");
        gbm1.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
        gb1.setItemMeta(gbm1);

        addLoot(new Loot(gb1, this, ItemRarity.RARE));

        ItemStack gb = new ItemStack(Material.GOLD_CHESTPLATE);
        ItemMeta gbm = gb.getItemMeta();
        gbm.setDisplayName("§5GODZILLA CHESTPLATTE");
        gbm.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
        gb.setItemMeta(gbm);

        addLoot(new Loot(gb, this, ItemRarity.RARE));
        
        addExecute(new AExecute(this) {
            @Override
            public void execute(Player player) {
                player.setMaxHealth(player.getMaxHealth() + 2);
            }
        });

        addExecute(new AExecute(this) {
            @Override
            public void execute(Player player) {
                player.setExp(player.getExp() + 5);
            }
        });

        this.winLoot = new ItemStack(Material.DIAMOND_CHESTPLATE);

    }

}
