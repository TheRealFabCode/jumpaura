package de.fab.luckyblocks.blocks;

import de.fab.enumList.ItemRarity;
import de.fab.luckyblocks.block.AExecute;
import de.fab.luckyblocks.block.ALuckyBlock;
import de.fab.luckyblocks.block.Loot;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

public class OpBlock extends ALuckyBlock {

    public OpBlock() {
        super("Op Lucky Block", "null");
    }

    @Override
    public void init() {
        addLoot(new Loot(new ItemStack(Material.IRON_SWORD), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.DIAMOND_SWORD), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.BOW), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.GOLDEN_APPLE, 1, (short)1), this, ItemRarity.ULTRARARE));
        addLoot(new Loot(new ItemStack(Material.BREAD, 3), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.DIAMOND_HELMET), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.DIAMOND_BOOTS), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.DIAMOND_LEGGINGS), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.IRON_LEGGINGS), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.IRON_HELMET), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.IRON_BOOTS), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.DIAMOND_CHESTPLATE), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.IRON_CHESTPLATE), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.ARROW, 6), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.PUMPKIN_PIE, 3) , this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.GOLDEN_APPLE, 2), this, ItemRarity.RARE));

        Potion potion = new Potion(PotionType.FIRE_RESISTANCE);

        ItemStack potionItem = potion.toItemStack(1);
        PotionMeta potionMeta = (PotionMeta) potionItem.getItemMeta();
        potionMeta.clearCustomEffects();
        potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 5, 0), true);
        potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.JUMP, 20 * 10, 0), true);
        potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 10, 2), true);
        potionMeta.setDisplayName(ChatColor.GOLD + "Lucky Potion");
        potionItem.setItemMeta(potionMeta);

        addLoot(new Loot(potionItem, this, ItemRarity.ULTRARARE));

        Potion potion2 = new Potion(PotionType.WEAKNESS);
        potion2.setSplash(true);
        ItemStack potionItem2 = potion2.toItemStack(1);
        PotionMeta potionMeta2 = (PotionMeta) potionItem2.getItemMeta();
        potionMeta2.clearCustomEffects();
        potionMeta2.addCustomEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 10, 1), true);
        potionMeta2.addCustomEffect(new PotionEffect(PotionEffectType.HARM, 20 * 10, 1), true);
        potionMeta2.addCustomEffect(new PotionEffect(PotionEffectType.HUNGER, 20 * 10, 1), true);
        potionMeta2.setDisplayName(ChatColor.DARK_PURPLE + "Unlucky Potion");
        potionItem2.setItemMeta(potionMeta2);

        ItemStack gh5 = new ItemStack(Material.BOW);
        gh5.addEnchantment(Enchantment.ARROW_DAMAGE, 3);
        ItemMeta gh15 = gh5.getItemMeta();
        gh15.setDisplayName("§9OP BOW");
        gh15.addEnchant(Enchantment.ARROW_DAMAGE, 3, true);
        gh5.setItemMeta(gh15);

        addLoot(new Loot(gh5, this, ItemRarity.ULTRARARE));

        ItemStack gl7 = new ItemStack(Material.DIAMOND_BOOTS);
        ItemMeta glh7 = gl7.getItemMeta();
        glh7.setDisplayName("§9OP BOOTS");
        glh7.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
        gl7.setItemMeta(glh7);

        addLoot(new Loot(gl7, this, ItemRarity.ULTRARARE));

        ItemStack gl8 = new ItemStack(Material.DIAMOND_LEGGINGS);
        ItemMeta glh8 = gl8.getItemMeta();
        glh8.setDisplayName("§9OP LEGGINS");
        glh8.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
        gl8.setItemMeta(glh8);

        addLoot(new Loot(gl8, this, ItemRarity.ULTRARARE));

        ItemStack gl87 = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemMeta glh87 = gl87.getItemMeta();
        glh87.setDisplayName("§9OP CHESTPLATE");
        glh87.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
        gl87.setItemMeta(glh87);

        addLoot(new Loot(gl87, this, ItemRarity.ULTRARARE));

        ItemStack gl97 = new ItemStack(Material.DIAMOND_HELMET);
        ItemMeta glh97 = gl97.getItemMeta();
        glh97.setDisplayName("§9OP HELMET");
        glh97.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
        gl97.setItemMeta(glh97);

        addLoot(new Loot(gl97, this, ItemRarity.ULTRARARE));

        ItemStack gl76 = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta glh76 = gl76.getItemMeta();
        glh76.setDisplayName("§9OP SWORD");
        glh76.addEnchant(Enchantment.DAMAGE_ALL, 3, true);
        gl76.setItemMeta(glh76);

        addLoot(new Loot(gl76, this, ItemRarity.ULTRARARE));

        addLoot(new Loot(potionItem2, this, ItemRarity.RARE));
        addExecute(new AExecute(this) {
            @Override
            public void execute(Player player) {
                player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0, false));
            }
        });
        addExecute(new AExecute(this) {
            @Override
            public void execute(Player player) {
                player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, Integer.MAX_VALUE, 0 , false));
            }
        });
        addExecute(new AExecute(this) {
            @Override
            public void execute(Player player) {
                player.setMaxHealth(player.getMaxHealth() + 4);
            }
        });
        addExecute(new AExecute(this) {
            @Override
            public void execute(Player player) {
                player.giveExp(5);
            }
        });

        this.winLoot = new ItemStack(Material.DIAMOND_CHESTPLATE);
    }

}
