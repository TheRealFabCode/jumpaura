package de.fab.luckyblocks.blocks;

import de.fab.JumpAura;
import de.fab.enumList.ItemRarity;
import de.fab.luckyblocks.block.ALuckyBlock;
import de.fab.luckyblocks.block.Loot;
import net.minecraft.server.v1_8_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class TitanBlock extends ALuckyBlock {


    public TitanBlock() {
        super("Titan Block", "");
    }

    @Override
    public void init() {
        ItemStack titanHelmet = new ItemStack(Material.DIAMOND_HELMET);
        ItemMeta trHM = titanHelmet.getItemMeta();
        trHM.setDisplayName("§9Titan Helmet");

        titanHelmet.setItemMeta(trHM);

        ItemStack titanChest = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemMeta trCM = titanChest.getItemMeta();
        trCM.setDisplayName("§9Titan Chestplate");
        titanChest.setItemMeta(trCM);

        ItemStack titanLeggings = new ItemStack(Material.DIAMOND_LEGGINGS);
        ItemMeta trlM = titanLeggings.getItemMeta();
        trlM.setDisplayName("§9Titan Leggings");
        titanLeggings.setItemMeta(trlM);

        ItemStack titanBoots = new ItemStack(Material.DIAMOND_BOOTS);
        ItemMeta trbM = titanBoots.getItemMeta();
        trbM.setDisplayName("§9Titan Boots");
        titanBoots.setItemMeta(trbM);

        ItemStack netherHelmet = new ItemStack(Material.IRON_HELMET);
        ItemMeta nHM = netherHelmet.getItemMeta();
        nHM.setDisplayName("§4Nether Helmet");
        netherHelmet.setItemMeta(nHM);

        ItemStack netherChest = new ItemStack(Material.IRON_CHESTPLATE);
        ItemMeta nCM = netherChest.getItemMeta();
        nCM.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, false);
        nCM.setDisplayName("§4Nether Chestplate");
        netherChest.setItemMeta(nCM);

        ItemStack netherLeggings = new ItemStack(Material.IRON_LEGGINGS);
        ItemMeta nlM = netherLeggings.getItemMeta();
        nlM.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false);
        nlM.setDisplayName("§4Nether Leggings");
        netherLeggings.setItemMeta(nlM);

        ItemStack netherBoots = new ItemStack(Material.IRON_BOOTS);
        ItemMeta nbM = netherBoots.getItemMeta();
        nbM.setDisplayName("§4Nether Boots");
        netherBoots.setItemMeta(nbM);

        ItemStack citrinHelmet = new ItemStack(Material.CHAINMAIL_HELMET);
        ItemMeta cHM = citrinHelmet.getItemMeta();
        cHM.setDisplayName("§aCitrin Helmet");
        citrinHelmet.setItemMeta(cHM);

        ItemStack citrinChest = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
        ItemMeta cCM = citrinChest.getItemMeta();
        cCM.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 0, false);
        cCM.setDisplayName("§aCitrin Chestplate");
        citrinChest.setItemMeta(cCM);

        ItemStack citrinLeggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
        ItemMeta clM = citrinLeggings.getItemMeta();
        clM.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 0, false);
        clM.setDisplayName("§aCitrin Leggings");
        citrinLeggings.setItemMeta(clM);

        ItemStack citrinBoots = new ItemStack(Material.CHAINMAIL_BOOTS);
        ItemMeta cbM = citrinBoots.getItemMeta();
        cbM.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 0, false);
        cbM.setDisplayName("§aCitrin Boots");
        citrinBoots.setItemMeta(cbM);

        ItemStack diamondApple = new ItemStack(Material.GOLDEN_APPLE);
        ItemMeta dM = diamondApple.getItemMeta();
        dM.setDisplayName("§bDiamond Apple");
        diamondApple.setItemMeta(dM);


        this.addLoot(new Loot(titanBoots, this, ItemRarity.RARE));
        this.addLoot(new Loot(titanLeggings, this, ItemRarity.RARE));
        this.addLoot(new Loot(titanChest, this, ItemRarity.RARE));
        this.addLoot(new Loot(titanHelmet, this, ItemRarity.RARE));

        this.addLoot(new Loot(citrinBoots, this, ItemRarity.USUALLY));
        this.addLoot(new Loot(citrinLeggings, this, ItemRarity.USUALLY));
        this.addLoot(new Loot(citrinChest, this, ItemRarity.USUALLY));
        this.addLoot(new Loot(citrinHelmet, this, ItemRarity.USUALLY));

        this.addLoot(new Loot(netherBoots, this, ItemRarity.RARE));
        this.addLoot(new Loot(netherLeggings, this, ItemRarity.RARE));
        this.addLoot(new Loot(netherChest, this, ItemRarity.RARE));
        this.addLoot(new Loot(netherHelmet, this, ItemRarity.RARE));

        this.addLoot(new Loot(diamondApple, this, ItemRarity.RARE));

        for (EffectSword effectSword : EffectSword.values()) {
            this.addLoot(new Loot(getTitanSword(effectSword), this, ItemRarity.RARE));
            this.addLoot(new Loot(getTitanGreatSword(effectSword), this, ItemRarity.ULTRARARE));
            this.addLoot(new Loot(getCitrinGreatSword(effectSword), this, ItemRarity.ULTRARARE));
            this.addLoot(new Loot(getNetherSword(effectSword), this, ItemRarity.RARE));
            this.addLoot(new Loot(getNetherGreatSword(effectSword), this, ItemRarity.ULTRARARE));
            this.addLoot(new Loot(getCitrinSword(effectSword), this, ItemRarity.RARE));
        }

        addLoot(new Loot(new ItemStack(Material.COOKED_BEEF, 4), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.GOLDEN_APPLE), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.BOW), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.ARROW, 3), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.COOKED_BEEF, 2), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.APPLE, 2), this, ItemRarity.RARE));

        this.winLoot = getTitanGreatSword(EffectSword.BLINDNESS2);

        new BukkitRunnable() {
            @Override
            public void run() {
                for (Player p : Bukkit.getOnlinePlayers()) {
                    float addHealth = 20f;
                    if (p.getInventory().getBoots() != null && p.getInventory().getBoots().getItemMeta().getDisplayName() != null && p.getInventory().getBoots().getItemMeta().getDisplayName().equalsIgnoreCase("§9Titan Boots")) {
                        addHealth += 1f;
                    }
                    if (p.getInventory().getLeggings() != null && p.getInventory().getLeggings().getItemMeta().getDisplayName() != null && p.getInventory().getLeggings().getItemMeta().getDisplayName().equalsIgnoreCase("§9Titan Leggings")) {
                        addHealth += 3f;
                    }
                    if (p.getInventory().getChestplate() != null && p.getInventory().getChestplate().getItemMeta().getDisplayName() != null && p.getInventory().getChestplate().getItemMeta().getDisplayName().equalsIgnoreCase("§9Titan Chestplate")) {
                        addHealth += 4f;
                    }
                    if (p.getInventory().getHelmet() != null && p.getInventory().getHelmet().getItemMeta().getDisplayName() != null && p.getInventory().getHelmet().getItemMeta().getDisplayName().equalsIgnoreCase("§9Titan Helmet")) {
                        addHealth += 2f;
                    }
                    p.setMaxHealth(addHealth);

                    if (p.getInventory().getBoots() != null && p.getInventory().getHelmet() != null && p.getInventory().getChestplate() != null && p.getInventory().getLeggings() != null && p.getInventory().getBoots().getItemMeta().getDisplayName() != null && p.getInventory().getHelmet().getItemMeta().getDisplayName() != null && p.getInventory().getChestplate().getItemMeta().getDisplayName() != null && p.getInventory().getLeggings().getItemMeta().getDisplayName() != null) {
                        if (p.getInventory().getBoots().getItemMeta().getDisplayName().equalsIgnoreCase("§aCitrin Boots") && p.getInventory().getLeggings().getItemMeta().getDisplayName().equalsIgnoreCase("§aCitrin Leggings") && p.getInventory().getChestplate().getItemMeta().getDisplayName().equalsIgnoreCase("§aCitrin Chestplate") && p.getInventory().getHelmet().getItemMeta().getDisplayName().equalsIgnoreCase("§aCitrin Helmet")) {
                            ArrayList<PotionEffect> toRemove = new ArrayList<>();
                            for (PotionEffect potionEffect : p.getActivePotionEffects()) {
                                if (potionEffect.getType() == PotionEffectType.BLINDNESS) {
                                    toRemove.add(potionEffect);
                                }
                                if (potionEffect.getType() == PotionEffectType.HARM) {
                                    toRemove.add(potionEffect);
                                }
                                if (potionEffect.getType() == PotionEffectType.HUNGER) {
                                    toRemove.add(potionEffect);
                                }
                                if (potionEffect.getType() == PotionEffectType.POISON) {
                                    toRemove.add(potionEffect);
                                }
                                if (potionEffect.getType() == PotionEffectType.WEAKNESS) {
                                    toRemove.add(potionEffect);
                                }
                                if (potionEffect.getType() == PotionEffectType.SLOW) {
                                    toRemove.add(potionEffect);
                                }
                                if (potionEffect.getType() == PotionEffectType.WITHER) {
                                    toRemove.add(potionEffect);
                                }
                            }
                            for (PotionEffect potionEffect : toRemove) {
                                p.getActivePotionEffects().remove(potionEffect);
                            }
                        }
                    }
                }
            }
        }.

                runTaskTimer(JumpAura.getInstance(), 20L, 1L);


    }

    public ItemStack getCitrinSword(EffectSword effectSword) {
        ItemStack citrinSword = new ItemStack(Material.IRON_SWORD);
        ItemMeta cSMeta = citrinSword.getItemMeta();
        cSMeta.setDisplayName("§aCitrin Sword - " + effectSword.name().toUpperCase() + "");
        citrinSword.setItemMeta(cSMeta);
        return citrinSword;
    }

    public ItemStack getTitanSword(EffectSword effectSword) {
        org.bukkit.inventory.ItemStack tsword = new org.bukkit.inventory.ItemStack(Material.DIAMOND_SWORD);
        ItemMeta sm = tsword.getItemMeta();
        sm.setDisplayName("§9Titan Sword - " + effectSword.name().toUpperCase());
        tsword.setItemMeta(sm);
        net.minecraft.server.v1_8_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(tsword);
        NBTTagCompound compound = nmsStack.hasTag() ? nmsStack.getTag() : new NBTTagCompound();
        NBTTagList modifiers = new NBTTagList();
        NBTTagCompound damage = new NBTTagCompound();
        damage.set("AttributeName", new NBTTagString("generic.attackDamage"));
        damage.set("Name", new NBTTagString("generic.attackDamage"));
        damage.set("Amount", new NBTTagDouble(8.0D));
        damage.set("Operation", new NBTTagInt(0));
        damage.set("UUIDLeast", new NBTTagInt(894654));
        damage.set("UUIDMost", new NBTTagInt(2872));
        damage.set("Slot", new NBTTagString("mainhand"));
        modifiers.add(damage);
        compound.set("AttributeModifiers", modifiers);
        compound.set("Unbreakable", new NBTTagByte((byte) 1));
        nmsStack.setTag(compound);
        return CraftItemStack.asBukkitCopy(nmsStack);
    }

    public ItemStack getTitanGreatSword(EffectSword effectSword) {
        org.bukkit.inventory.ItemStack tgreat = new org.bukkit.inventory.ItemStack(Material.DIAMOND_SWORD);
        ItemMeta tgm = tgreat.getItemMeta();
        tgm.setDisplayName("§9Titan Greatsword - " + effectSword.name().toUpperCase() + "");
        tgreat.setItemMeta(tgm);
        net.minecraft.server.v1_8_R1.ItemStack nmsStack2 = CraftItemStack.asNMSCopy(tgreat);
        NBTTagCompound compound2 = nmsStack2.hasTag() ? nmsStack2.getTag() : new NBTTagCompound();
        NBTTagList modifiers2 = new NBTTagList();
        NBTTagCompound damage2 = new NBTTagCompound();
        damage2.set("AttributeName", new NBTTagString("generic.attackDamage"));
        damage2.set("Name", new NBTTagString("generic.attackDamage"));
        damage2.set("Amount", new NBTTagDouble(11.0D));
        damage2.set("Operation", new NBTTagInt(0));
        damage2.set("UUIDLeast", new NBTTagInt(894654));
        damage2.set("UUIDMost", new NBTTagInt(2872));
        damage2.set("Slot", new NBTTagString("mainhand"));
        modifiers2.add(damage2);
        compound2.set("AttributeModifiers", modifiers2);
        compound2.set("Unbreakable", new NBTTagByte((byte) 1));
        nmsStack2.setTag(compound2);
        return CraftItemStack.asBukkitCopy(nmsStack2);
    }

    public ItemStack getCitrinGreatSword(EffectSword effectSword) {
        org.bukkit.inventory.ItemStack tgreat = new org.bukkit.inventory.ItemStack(Material.IRON_SWORD);
        ItemMeta tgm = tgreat.getItemMeta();
        tgm.setDisplayName("§aCitrin Greatsword - " + effectSword.name().toUpperCase() + "");
        tgreat.setItemMeta(tgm);
        net.minecraft.server.v1_8_R1.ItemStack nmsStack2 = CraftItemStack.asNMSCopy(tgreat);
        NBTTagCompound compound2 = nmsStack2.hasTag() ? nmsStack2.getTag() : new NBTTagCompound();
        NBTTagList modifiers2 = new NBTTagList();
        NBTTagCompound damage2 = new NBTTagCompound();
        damage2.set("AttributeName", new NBTTagString("generic.attackDamage"));
        damage2.set("Name", new NBTTagString("generic.attackDamage"));
        damage2.set("Amount", new NBTTagDouble(8.5D));
        damage2.set("Operation", new NBTTagInt(0));
        damage2.set("UUIDLeast", new NBTTagInt(894654));
        damage2.set("UUIDMost", new NBTTagInt(2872));
        damage2.set("Slot", new NBTTagString("mainhand"));
        modifiers2.add(damage2);
        compound2.set("AttributeModifiers", modifiers2);
        compound2.set("Unbreakable", new NBTTagByte((byte) 1));
        nmsStack2.setTag(compound2);
        return CraftItemStack.asBukkitCopy(nmsStack2);
    }

    public ItemStack getNetherSword(EffectSword effectSword) {
        org.bukkit.inventory.ItemStack tgreat = new org.bukkit.inventory.ItemStack(Material.STONE_SWORD);
        ItemMeta tgm = tgreat.getItemMeta();
        tgm.setDisplayName("§4Nether Sword - " + effectSword.name().toUpperCase() + "");
        tgreat.setItemMeta(tgm);
        net.minecraft.server.v1_8_R1.ItemStack nmsStack2 = CraftItemStack.asNMSCopy(tgreat);
        NBTTagCompound compound2 = nmsStack2.hasTag() ? nmsStack2.getTag() : new NBTTagCompound();
        NBTTagList modifiers2 = new NBTTagList();
        NBTTagCompound damage2 = new NBTTagCompound();
        damage2.set("AttributeName", new NBTTagString("generic.attackDamage"));
        damage2.set("Name", new NBTTagString("generic.attackDamage"));
        damage2.set("Amount", new NBTTagDouble(6.5D));
        damage2.set("Operation", new NBTTagInt(0));
        damage2.set("UUIDLeast", new NBTTagInt(894654));
        damage2.set("UUIDMost", new NBTTagInt(2872));
        damage2.set("Slot", new NBTTagString("mainhand"));
        modifiers2.add(damage2);
        compound2.set("AttributeModifiers", modifiers2);
        compound2.set("Unbreakable", new NBTTagByte((byte) 1));
        nmsStack2.setTag(compound2);
        return CraftItemStack.asBukkitCopy(nmsStack2);
    }

    public ItemStack getNetherGreatSword(EffectSword effectSword) {
        org.bukkit.inventory.ItemStack tgreat = new org.bukkit.inventory.ItemStack(Material.STONE_SWORD);
        ItemMeta tgm = tgreat.getItemMeta();
        tgm.setDisplayName("§4Nether Greatsword - " + effectSword.name().toUpperCase() + "");
        tgreat.setItemMeta(tgm);
        net.minecraft.server.v1_8_R1.ItemStack nmsStack2 = CraftItemStack.asNMSCopy(tgreat);
        NBTTagCompound compound2 = nmsStack2.hasTag() ? nmsStack2.getTag() : new NBTTagCompound();
        NBTTagList modifiers2 = new NBTTagList();
        NBTTagCompound damage2 = new NBTTagCompound();
        damage2.set("AttributeName", new NBTTagString("generic.attackDamage"));
        damage2.set("Name", new NBTTagString("generic.attackDamage"));
        damage2.set("Amount", new NBTTagDouble(9D));
        damage2.set("Operation", new NBTTagInt(0));
        damage2.set("UUIDLeast", new NBTTagInt(894654));
        damage2.set("UUIDMost", new NBTTagInt(2872));
        damage2.set("Slot", new NBTTagString("mainhand"));
        modifiers2.add(damage2);
        compound2.set("AttributeModifiers", modifiers2);
        compound2.set("Unbreakable", new NBTTagByte((byte) 1));
        nmsStack2.setTag(compound2);
        return CraftItemStack.asBukkitCopy(nmsStack2);
    }

    public enum EffectSword {
        POISON, BLINDNESS, WEAKNESS, SLOWNESS, WITHER, POISON2, BLINDNESS2, WEAKNESS2, SLOWNESS2, WITHER2, NONE
    }
}


