package de.fab.luckyblocks.blocks;

import de.fab.JumpAura;
import de.fab.enumList.ItemRarity;
import de.fab.luckyblocks.block.ALuckyBlock;
import de.fab.luckyblocks.block.Loot;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class HeroBlock extends ALuckyBlock {

    public HeroBlock() {
        super("Hero Block", "");
    }

    @Override
    public void init() {
        addLoot(new Loot(new ItemStack(Material.IRON_SWORD), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.DIAMOND_SWORD), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.APPLE, 2), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.COOKED_BEEF, 2), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.GOLDEN_APPLE), this, ItemRarity.ULTRARARE));
        addLoot(new Loot(new ItemStack(Material.BOW), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.ARROW, 6), this, ItemRarity.USUALLY));
        addLoot(new Loot(new ItemStack(Material.COOKED_BEEF, 2), this, ItemRarity.RARE));
        addLoot(new Loot(new ItemStack(Material.APPLE, 2), this, ItemRarity.RARE));

        //Item Recycler muss hinzugefügt werden



        ItemStack heroHelmet = new ItemStack(Material.DIAMOND_HELMET);
        ItemMeta hHM = heroHelmet.getItemMeta();
        hHM.setDisplayName("§5Hero Helmet");
        heroHelmet.setItemMeta(hHM);

        ItemStack heroChest = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemMeta hCM = heroChest.getItemMeta();
        hCM.setDisplayName("§5Hero Chestplate");
        heroChest.setItemMeta(hCM);

        ItemStack heroLeggings = new ItemStack(Material.DIAMOND_LEGGINGS);
        ItemMeta hlM = heroLeggings.getItemMeta();
        hlM.setDisplayName("§5Hero Leggings");
        heroLeggings.setItemMeta(hlM);

        ItemStack heroBoots = new ItemStack(Material.DIAMOND_BOOTS);
        ItemMeta hbM = heroBoots.getItemMeta();
        hbM.setDisplayName("§5Hero Boots");
        heroBoots.setItemMeta(hbM);

        this.addLoot(new Loot(heroBoots, this, ItemRarity.RARE));
        this.addLoot(new Loot(heroChest, this, ItemRarity.RARE));
        this.addLoot(new Loot(heroLeggings, this, ItemRarity.RARE));
        this.addLoot(new Loot(heroHelmet, this, ItemRarity.RARE));

        ItemStack thiefHelmet = new ItemStack(Material.DIAMOND_HELMET);
        ItemMeta tHM = heroHelmet.getItemMeta();
        tHM.setDisplayName("§bThief Helmet");
        thiefHelmet.setItemMeta(tHM);

        ItemStack thiefChest = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemMeta tCM = heroChest.getItemMeta();
        tCM.setDisplayName("§bThief Chestplate");
        thiefChest.setItemMeta(tCM);

        ItemStack thiefLeggings = new ItemStack(Material.DIAMOND_LEGGINGS);
        ItemMeta tlM = heroLeggings.getItemMeta();
        tlM.setDisplayName("§bThief Leggings");
        thiefLeggings.setItemMeta(tlM);

        ItemStack thiefBoots = new ItemStack(Material.DIAMOND_BOOTS);
        ItemMeta tbM = heroBoots.getItemMeta();
        tbM.setDisplayName("§bThief Boots");
        thiefBoots.setItemMeta(tbM);

        this.addLoot(new Loot(thiefBoots, this, ItemRarity.RARE));
        this.addLoot(new Loot(thiefChest, this, ItemRarity.RARE));
        this.addLoot(new Loot(thiefLeggings, this, ItemRarity.RARE));
        this.addLoot(new Loot(thiefHelmet, this, ItemRarity.RARE));

        ItemStack tribalHelmet = new ItemStack(Material.DIAMOND_HELMET);
        ItemMeta trHM = tribalHelmet.getItemMeta();
        trHM.setDisplayName("§bTribal Helmet");
        tribalHelmet.setItemMeta(trHM);

        ItemStack tribalChest = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemMeta trCM = tribalChest.getItemMeta();
        trCM.setDisplayName("§bTribal Chestplate");
        tribalChest.setItemMeta(trCM);

        ItemStack tribalLeggings = new ItemStack(Material.DIAMOND_LEGGINGS);
        ItemMeta trlM = tribalLeggings.getItemMeta();
        trlM.setDisplayName("§bTribal Leggings");
        tribalLeggings.setItemMeta(trlM);

        ItemStack tribalBoots = new ItemStack(Material.DIAMOND_BOOTS);
        ItemMeta trbM = tribalBoots.getItemMeta();
        trbM.setDisplayName("§bTribal Boots");
        tribalBoots.setItemMeta(trbM);

        this.addLoot(new Loot(tribalBoots, this, ItemRarity.RARE));
        this.addLoot(new Loot(tribalChest, this, ItemRarity.RARE));
        this.addLoot(new Loot(tribalLeggings, this, ItemRarity.RARE));
        this.addLoot(new Loot(tribalHelmet, this, ItemRarity.RARE));

        new BukkitRunnable(){
            @Override
            public void run(){
                for(Player p : Bukkit.getOnlinePlayers()){
                    if(p.getInventory().getBoots() != null && p.getInventory().getHelmet() != null &&  p.getInventory().getChestplate() != null && p.getInventory().getLeggings() != null && p.getInventory().getBoots().getItemMeta().getDisplayName() != null && p.getInventory().getHelmet().getItemMeta().getDisplayName() != null &&  p.getInventory().getChestplate().getItemMeta().getDisplayName() != null && p.getInventory().getLeggings().getItemMeta().getDisplayName() != null) {
                        if(p.getInventory().getBoots().getItemMeta().getDisplayName().equalsIgnoreCase("§5Hero Boots") && p.getInventory().getLeggings().getItemMeta().getDisplayName().equalsIgnoreCase("§5Hero Leggings") && p.getInventory().getChestplate().getItemMeta().getDisplayName().equalsIgnoreCase("§5Hero Chestplate")&& p.getInventory().getHelmet().getItemMeta().getDisplayName().equalsIgnoreCase("§5Hero Helmet")){
                            p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20, 0, true,true));
                            p.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 20, 1, true,true));
                            p.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 20, 0, true,true));
                        }
                        if(p.getInventory().getBoots().getItemMeta().getDisplayName().equalsIgnoreCase("§bThief Boots") && p.getInventory().getLeggings().getItemMeta().getDisplayName().equalsIgnoreCase("§bThief Leggings") && p.getInventory().getChestplate().getItemMeta().getDisplayName().equalsIgnoreCase("§bThief Chestplate")&& p.getInventory().getHelmet().getItemMeta().getDisplayName().equalsIgnoreCase("§bThief Helmet")){
                            p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20, 0, true,true));
                            p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20, 1, true,true));
                        }
                        if(p.getInventory().getBoots().getItemMeta().getDisplayName().equalsIgnoreCase("§bTribal Boots") && p.getInventory().getLeggings().getItemMeta().getDisplayName().equalsIgnoreCase("§bTribal Leggings") && p.getInventory().getChestplate().getItemMeta().getDisplayName().equalsIgnoreCase("§bTribal Chestplate")&& p.getInventory().getHelmet().getItemMeta().getDisplayName().equalsIgnoreCase("§bTribal Helmet")){
                            p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20, 0, true,true));
                            p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 20, 2, true,true));
                        }
                    }
                }
            }
        }.runTaskTimer(JumpAura.getInstance(), 20L, 1L);

        ItemStack enderSword = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta enderMeta = enderSword.getItemMeta();
        enderMeta.setDisplayName("§6Endersword");
        enderMeta.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
        enderSword.setItemMeta(enderMeta);

        this.winLoot = enderSword;

    }



}
