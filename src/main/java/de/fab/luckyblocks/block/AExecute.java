package de.fab.luckyblocks.block;

import org.bukkit.entity.Player;

public abstract class AExecute {
    
    private ALuckyBlock block;
    
    public AExecute(ALuckyBlock block){
        this.block = block;
    }
    
    public abstract void execute(Player player);
    
}
