package de.fab.luckyblocks.block;

import java.util.ArrayList;
import java.util.Random;

import de.fab.enumList.ItemRarity;
import de.fab.utils.GeneralCore;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class ALuckyBlock extends GeneralCore {

    private String name;
    private String pack;
    public ItemStack winLoot;
    private ArrayList<Loot> lootlist = new ArrayList<Loot>();
    private ArrayList<AExecute> exlist = new ArrayList<AExecute>();

    public ALuckyBlock(String name, String pack) {
        this.name = name;
        this.pack = pack;
        init();
    }

    public abstract void init();

    public void addLoot(Loot l) {
        lootlist.add(l);
    }

    public void addExecute(AExecute ex) {
        exlist.add(ex);
    }

    public void onBreak(Player player) {
        Random r = new Random();
        int ra = r.nextInt(100);
        if(this.exlist.isEmpty()){
            dropLoot(player);
        } else {
            if(ra > 10){
                dropLoot(player);
            } else {
                this.exlist.get(new Random().nextInt(exlist.size())).execute(player);
            }
        }


    }

    private void dropLoot(Player player) {
        int randomNum = new Random().nextInt(100);
        if(randomNum < 50 && randomNum >= 0){
            getLootByRarity(ItemRarity.USUALLY).get(new Random().nextInt(getLootByRarity(ItemRarity.USUALLY).size())).drop(player);
        }
        if(randomNum < 85 && randomNum >= 50){
            getLootByRarity(ItemRarity.RARE).get(new Random().nextInt(getLootByRarity(ItemRarity.RARE).size())).drop(player);
        }
        if(randomNum <= 100 && randomNum >= 85){
            getLootByRarity(ItemRarity.ULTRARARE).get(new Random().nextInt(getLootByRarity(ItemRarity.ULTRARARE).size())).drop(player);
        }
    }

    public ItemStack getWinLoot(){ return this.winLoot; }

    public String getName() {
        return name;
    }

    public String getPack() {
        return pack;
    }

    public boolean isArmor(ItemStack itemStack){
        boolean r = false;
        if(itemStack.getType() == Material.DIAMOND_BOOTS){
            r = true;
        }
        if(itemStack.getType() == Material.DIAMOND_HELMET){
            r = true;
        }
        if(itemStack.getType() == Material.DIAMOND_LEGGINGS){
            r = true;
        }
        if(itemStack.getType() == Material.DIAMOND_CHESTPLATE){
            r = true;
        }
        if(itemStack.getType() == Material.IRON_BOOTS){
            r = true;
        }
        if(itemStack.getType() == Material.IRON_HELMET){
            r = true;
        }
        if(itemStack.getType() == Material.IRON_LEGGINGS){
            r = true;
        }
        if(itemStack.getType() == Material.IRON_CHESTPLATE){
            r = true;
        }
        if(itemStack.getType() == Material.GOLD_BOOTS){
            r = true;
        }
        if(itemStack.getType() == Material.GOLD_HELMET){
            r = true;
        }
        if(itemStack.getType() == Material.GOLD_LEGGINGS){
            r = true;
        }
        if(itemStack.getType() == Material.GOLD_CHESTPLATE){
            r = true;
        }
        if(itemStack.getType() == Material.LEATHER_BOOTS){
            r = true;
        }
        if(itemStack.getType() == Material.LEATHER_HELMET){
            r = true;
        }
        if(itemStack.getType() == Material.LEATHER_LEGGINGS){
            r = true;
        }
        if(itemStack.getType() == Material.LEATHER_CHESTPLATE){
            r = true;
        }
        if(itemStack.getType() == Material.CHAINMAIL_BOOTS){
            r = true;
        }
        if(itemStack.getType() == Material.CHAINMAIL_HELMET){
            r = true;
        }
        if(itemStack.getType() == Material.CHAINMAIL_LEGGINGS){
            r = true;
        }
        if(itemStack.getType() == Material.CHAINMAIL_CHESTPLATE){
            r = true;
        }
        return r;
    }

    public boolean isSword(ItemStack itemStack){
        boolean r = false;
        if(itemStack.getType() == Material.DIAMOND_SWORD){
            r = true;
        }
        if(itemStack.getType() == Material.IRON_SWORD){
            r = true;
        }
        if(itemStack.getType() == Material.STONE_SWORD){
            r = true;
        }
        if(itemStack.getType() == Material.GOLD_SWORD){
            r = true;
        }
        if(itemStack.getType() == Material.WOOD_SWORD){
            r = true;
        }
        return r;
    }

    public boolean isFood(ItemStack itemStack){
        return itemStack.getType().isEdible() && itemStack.getType() != Material.GOLDEN_APPLE;
    }

    public boolean isArrow(ItemStack itemStack){
        return itemStack.getType() == Material.ARROW;
    }

    public boolean isBow(ItemStack itemStack){
        return itemStack.getType() == Material.BOW;
    }

    public boolean isGoldenApple(ItemStack itemStack){
        return itemStack.getType() == Material.GOLDEN_APPLE;
    }

    public ArrayList<Loot> getLootByRarity(ItemRarity rarity){
        ArrayList<Loot> newList = new ArrayList<>();
        for(Loot l : lootlist){
            if(l.getItemRarity() == rarity){
                newList.add(l);
            }
        }
        return newList;
    }

}
