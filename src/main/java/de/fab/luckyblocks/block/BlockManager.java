package de.fab.luckyblocks.block;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Random;

public class BlockManager {

    @Getter
    private ArrayList<ALuckyBlock> blockslist = new ArrayList<>();
    private ALuckyBlock chosenblock;

    public BlockManager() {}

    public void addBlock(ALuckyBlock block) {
        blockslist.add(block);
    }

    public void chooseBlock() {
        Random r = new Random();
        int random = r.nextInt(blockslist.size());
        chosenblock = blockslist.get(random);
    }
    
    public ALuckyBlock getBlock(){
        return chosenblock;
    }

    public ALuckyBlock getLuckyBlockByName(String name){
        for(ALuckyBlock luckyBlock : blockslist){
            if(luckyBlock.getName().equalsIgnoreCase(name)) {
                return luckyBlock;
            }
        }
        return null;
    }

    public void setBlock(ALuckyBlock luckyBlock){
        this.chosenblock = luckyBlock;
    }

}
