/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fab.luckyblocks.block;

import de.fab.enumList.ItemRarity;
import de.fab.luckyblocks.block.ALuckyBlock;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Loot {
    
    public ItemStack item;
    public ALuckyBlock block;
    @Getter
    public ItemRarity itemRarity;
    
    public Loot(ItemStack item, ALuckyBlock block, ItemRarity rarity){
        this.block = block;
        this.item = item;
        this.itemRarity = rarity;
        init();
    }
    
    private void init(){
        block.addLoot(this);
    }
    
    public void drop(Player player){
        player.getInventory().addItem(item);
    }
    
}
