package de.fab.commands.initial;

import de.fab.utils.GeneralCore;
import de.fab.utils.Manager;
import lombok.Data;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Fabian on 11.02.2017.
 */
@Data
public class SetSpawn extends GeneralCore implements CommandExecutor {

    @Override
    public boolean onCommand( CommandSender cs, Command command, String s, String[] args ) {
        if ( cs instanceof Player ) {
            Player p = (Player) cs;
            if ( p.hasPermission( "games.admin" ) ) {
                if ( args[0].equalsIgnoreCase( "lobbyspawn" ) ) {
                    Manager.getInstance().getLcfg().getLobbySpawn().setLocation( p.getLocation() );
                    message( p, defaultTranslation().getSpawn_setlobby() );
                    save();
                    //p.sendMessage(JumpAura.getMainConfig().getPrefix() + Dictionary.getInstance().getDefaultTranslation().getSpawn_setlobby());
                } else if ( args[0].equalsIgnoreCase( "gamespawn" ) ) {
                    Manager.getInstance().getLcfg().getGameSpawn().setLocation( p.getLocation() );
                    message( p, defaultTranslation().getSpawn_setgame() );
                    save();
                    //p.sendMessage(JumpAura.getMainConfig().getPrefix() + Dictionary.getInstance().getDefaultTranslation().getSpawn_setgame());
                } else if ( args[0].equalsIgnoreCase( "fightspawn" ) ) {
                    Manager.getInstance().getLcfg().getFightSpawn().setLocation( p.getLocation() );
                    message( p, defaultTranslation().getSpawn_setfight() );
                    save();
                    //p.sendMessage(JumpAura.getMainConfig().getPrefix() + Dictionary.getInstance().getDefaultTranslation().getSpawn_setfight());
                } else {
                    message( p, defaultTranslation().getWrite_error() );
                    //p.sendMessage(JumpAura.getMainConfig().getPrefix() + Dictionary.getInstance().getDefaultTranslation().getWrite_error());
                }
                if ( Manager.getInstance().getLcfg().isConfigured() ) {
                    Bukkit.shutdown();
                }
            }
        }
        return false;

    }

    private void save() {
        try {
            Manager.getInstance().getLcfg().save();
        } catch ( InvalidConfigurationException ex ) {
            Logger.getLogger( SetSpawn.class.getName() ).log( Level.SEVERE, null, ex );
        }
    }
}
