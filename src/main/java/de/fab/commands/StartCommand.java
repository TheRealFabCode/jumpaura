package de.fab.commands;

import de.fab.enumList.GameStats;
import de.fab.utils.Manager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StartCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player){
            Player p = (Player) commandSender;
            if(p.hasPermission("together.team") || p.hasPermission("together.youtube")){
                if(Manager.getInstance().getCurrentStat() == GameStats.LOBBY && Manager.getInstance().getTimer().started()){
                    Manager.getInstance().getTimer().forceStart();
                    return true;
                }
            }
        }
        return false;
    }
}
