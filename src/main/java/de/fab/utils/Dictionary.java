package de.fab.utils;


import de.fab.config.Translation;
import net.cubespace.Yamler.Config.InvalidConfigurationException;

import java.io.File;
import java.util.HashMap;
import java.util.Vector;

/**
 * Project: buildingrecoded
 * Package: eu.cc.juyas.building.manager
 *
 * @author Drayke
 * @since 31.10.2016.
 */
public final class Dictionary extends GeneralCore
{
    private static Dictionary ourInstance = new Dictionary();

    public static Dictionary getInstance()
    {
        return ourInstance;
    }

    private Dictionary()
    {
    }

    private static HashMap<String, Translation> translations = new HashMap<>();

    /**
     * Loads languages from "lang" folder
     *
     * @throws InvalidConfigurationException
     */
    public void loadLanguages() throws InvalidConfigurationException
    {

        File dataFolder = plugin().getDataFolder();
        File file = new File(dataFolder.getAbsolutePath() + "/lang");
        if (file.exists())
            for (String translationKey : file.list())
            {
                debug("translationKey:" + translationKey);
                Translation tr = new Translation(plugin(), translationKey);
                tr.init();
                translations.put(translationKey, tr);
            }

        if (translations.size() == 0)
            loadDefault();

        String defaultLanguage = config().getDefaultLanguage();
        if (!translations.containsKey(defaultLanguage))
            console("[Lang] §cDid not found default language key: " + defaultLanguage);
    }

    /**
     * Loads default language file (on first load)
     *
     * @throws InvalidConfigurationException
     */
    private void loadDefault() throws InvalidConfigurationException
    {
        debug("Loaded default Language.");
        Translation tr = new Translation(plugin(), "de");
        tr.init();
        translations.put("de", tr);
    }

    /**
     * Get the translation file
     *
     * @param translationKey "en/de/fr/.."
     * @return Translation file
     */
    public Translation getTranslation(String translationKey)
    {
        if (translationKey == null || !translations.containsKey(translationKey))
        {
            console("Language key:" + translationKey + " not found. Uses default instead!");
            if (translations.containsKey(config().getDefaultLanguage()))
                return translations.get(config().getDefaultLanguage());
            else
            {
                console("Default Language key:" + config().getDefaultLanguage() + " not found. Uses last fallback!");
                try
                {
                    return translations.values().iterator().next();
                }
                catch (Exception e)
                {
                    return new Translation();
                }
            }
        }

        return translations.get(translationKey);

    }

    /**
     * Gets default Translation
     *
     * @return default Translation
     */
    public Translation getDefaultTranslation()
    {
        if (translations.containsKey(config().getDefaultLanguage()))
            return translations.get(config().getDefaultLanguage());
        else
        {
            console("Default Language key:" + config().getDefaultLanguage() + " not found. Uses last fallback!");
            return translations.values().iterator().next();
        }
    }

    /**
     * Get the available and loaded languages
     *
     * @return Vector with languages
     */
    public Vector<String> getAvailableLanguages()
    {
        Vector<String> languages = new Vector<>();
        for (Translation tr : translations.values())
        {
            languages.add(tr.getLanguage());
            debug(tr.getLanguage());
        }
        return languages;
    }

    public Vector<String> getAvailableLanguageKeys()
    {
        Vector<String> languages = new Vector<>();
        for (String entry : translations.keySet())
        {
            languages.add(entry);
        }
        return languages;
    }

}
