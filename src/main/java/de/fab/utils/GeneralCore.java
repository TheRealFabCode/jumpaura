package de.fab.utils;

import de.fab.JumpAura;
import de.fab.config.MainConfig;
import de.fab.config.Translation;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Juyas on 16.01.2017.
 */
public class GeneralCore 
{

    /**
     * Sends a message to a player or the console
     *
     * @param sender  the receiver of the message
     * @param message the message to be sent
     */
    protected final void message(CommandSender sender, boolean prefix, String... message)
    {
        String msg = prefix ? config().getPrefix() + " " : "";
        for (String s : message)
            msg += s;
        msg = ChatColor.translateAlternateColorCodes('&', msg);
        sender.sendMessage(msg);
    }

    protected final void message(CommandSender sender, String... message)
    {
        if (sender == null) return;
        message(sender, true, message);
    }

    protected final void console(String... message)
    {
        message(Bukkit.getConsoleSender(), message);
    }

    protected final void unpermitted(CommandSender cs)
    {
        cs.sendMessage("§7[§4!§7] §cYou are not permitted to do that!");
    }

    protected final void unpermitted(CommandSender cs, String permission)
    {
        cs.sendMessage("§7[§4!§7] §cYou are not permitted to do that! §7(" + permission + ")");
    }

    protected final void warn(CommandSender sender, String message)
    {
        sender.sendMessage("§7[§4!§7] §c" + message);
    }

    /**
     * Converts an String array into an List object
     *
     * @param list the array of String objects
     * @return a List object containing the array values
     */
    protected final List<String> list(String... list)
    {
        if (list == null || list.length == 0) return new ArrayList<>();
        Arrays.sort(list);
        return Arrays.asList(list);
    }

    /**
     * Shall send an help manual
     *
     * @param cs   the command sender
     * @param info information string list
     */
    protected final void help(CommandSender cs, String... info)
    {
        cs.sendMessage("§m------------------------------------");
        for (String s : info)
        {
            cs.sendMessage("§6[§7?§6] §7" + s);
        }
    }

    protected final void debug(Object... info)
    {
        if (config().isDebug())
        {
            Bukkit.getConsoleSender().sendMessage("§9[§7DEBUG:JUMPAURA§9]");
            String msg = "";
            for (Object o : info)
            {
                if (o != null)
                    msg += " §c| §7" + o.toString();
            }
            Bukkit.getConsoleSender().sendMessage(msg);
        }
    }
    protected final static void cmd(String cmd) {
        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), cmd);
    }

    protected final boolean isInt(String value)
    {
        if (value == null) return false;
        try
        {
            Integer.parseInt(value);
            return true;
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }

    protected String[] breakString(String string, String breaker)
    {
        String[] broken = new String[2];
        broken[0] = string.substring(0,string.indexOf(breaker));
        broken[1] = string.substring(string.indexOf(breaker)+breaker.length(),string.length());
        return broken;
    }

    protected final Translation defaultTranslation()
    {
        return Dictionary.getInstance().getDefaultTranslation();
    }

    protected final JumpAura plugin() {
        return JumpAura.getInstance();
    }

    protected final MainConfig config()
    {
        return JumpAura.getMainConfig();
    }

    protected final String uncolor(String text)
    {
        return ChatColor.stripColor(text);
    }

    protected final String recolor(String text)
    {
        return ChatColor.translateAlternateColorCodes('&', text);
    }

}
