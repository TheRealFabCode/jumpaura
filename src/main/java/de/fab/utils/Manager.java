package de.fab.utils;

import de.fab.enumList.GameStats;
import de.fab.config.LocationConfig;
import java.util.ArrayList;
import lombok.Getter;
import lombok.Setter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import org.bukkit.entity.Player;

/**
 * Created by Fabian on 07.02.2017.
 */
public class Manager extends GeneralCore {

    @Getter
    private static Manager instance = new Manager();
    
    @Getter
    private Timer timer = new Timer();
    
    @Getter
    private ArrayList<Player> players = new ArrayList<Player>();
    
    @Getter
    @Setter
    private boolean damage = true;
    
    @Getter
    private ArrayList<Player> spectators = new ArrayList<Player>();
    
    @Getter
    private LocationConfig lcfg = new LocationConfig(plugin());
    
    private Manager(){
        currentStat = GameStats.LOBBY;
        try {
            lcfg.init();
        } catch (InvalidConfigurationException e) {
            console(e.getMessage());
            e.printStackTrace();
        }
    }

    @Getter
    @Setter
    private GameStats currentStat;

    public boolean isStat(GameStats stat)
    {
        return currentStat.equals(stat);
    }
    
    public void addPlayer(Player p){
        players.add(p);
    }
    
    public void addSpec(Player p){
        spectators.add(p);
    }
    
    public void removePlayer(Player p){
        players.remove(p);
    }




}
