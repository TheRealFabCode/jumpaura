package de.fab.utils;

import de.fab.enumList.GameStats;
import de.fab.JumpAura;
import de.fab.luckyblocks.block.ALuckyBlock;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

public class Timer extends GeneralCore {

    private static int counter = (20 * 61);
    public static boolean ticking = false;
    private static int taskid;

    public Timer() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(JumpAura.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (!ticking) {
                    return;
                }
                if (Manager.getInstance().isStat(GameStats.LOBBY) && ticking) {
                    if (counter == 20 * 60 || counter == 20 * 30 || counter == 20 * 40 || counter == 20 * 50 || counter == 20 * 20 || (counter <= 10 * 20 && counter > 0)) {
                        for (Player p : Manager.getInstance().getPlayers()) {
                            message(p, defaultTranslation().getGame_start_1() + ChatColor.BLUE + counter / 20 + defaultTranslation().getGame_start_2());
                        }
                    }
                    if (counter == 0) {

                        HashMap<ALuckyBlock, Integer> counts = new HashMap<ALuckyBlock, Integer>();

                        for(ALuckyBlock block : JumpAura.getVoteList().values()){
                            if(counts.containsKey(block)){
                                int value = counts.get(block);
                                value++;
                                counts.remove(block);
                                counts.put(block, value);
                            } else {
                                counts.put(block, 1);
                            }
                        }

                        ALuckyBlock highestBlock = null;
                        int highestVote = 0;
                        for(ALuckyBlock aLuckyBlock : counts.keySet()){
                            if(counts.get(aLuckyBlock) >= highestVote){
                                highestBlock = aLuckyBlock;
                                highestVote = counts.get(aLuckyBlock);
                            }
                        }
                        if(highestBlock == null){
                            JumpAura.getBm().chooseBlock();
                        } else {
                            JumpAura.getBm().setBlock(highestBlock);
                        }

                        for (Player p : Manager.getInstance().getPlayers()) {
                            message(p, defaultTranslation().getGame_starting());
                            p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 20*60*200, 5, false, false));
                            p.getInventory().clear();
                            p.getInventory().addItem(new ItemStack(Material.WOOD_AXE));
                            p.teleport(Manager.getInstance().getLcfg().getGameSpawn().getLocation());
                            p.setGameMode(GameMode.SURVIVAL);
                            message(p, "Der ausgewählte Block ist " + JumpAura.getBm().getBlock().getName());
                        }

                        Manager.getInstance().getLcfg().getFightSpawn().getLocation().getWorld().setDifficulty(Difficulty.NORMAL);
                        ticking = false;
                        Manager.getInstance().setCurrentStat(GameStats.INGAME);
                        Bukkit.getScheduler().cancelTask(taskid);


                    }
                }
                if (Manager.getInstance().isStat(GameStats.INGAME) && ticking) {
                    if (counter <= 10 * 20 && counter > 0) {
                        for (Player p : Manager.getInstance().getPlayers()) {
                            message(p, defaultTranslation().getFight_tp_1() + ChatColor.BLUE + counter / 20 + defaultTranslation().getFight_tp_2());
                        }
                    }
                    if (counter == 0) {
                        for (Player p : Manager.getInstance().getPlayers()) {
                            p.removePotionEffect(PotionEffectType.JUMP);
                            p.teleport(Manager.getInstance().getLcfg().getFightSpawn().getLocation());
                            message(p, defaultTranslation().getNodamage_period());
                        }
                        for(Player p : Manager.getInstance().getSpectators()){
                            p.teleport(Manager.getInstance().getLcfg().getFightSpawn().getLocation());
                        }
                        Manager.getInstance().setDamage(false);
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                for (Player p : Manager.getInstance().getPlayers()) {
                                    message(p, defaultTranslation().getNodamage_period_end());
                                }
                                Manager.getInstance().setDamage(true);
                            }
                        }.runTaskLater(JumpAura.getInstance(), 20 * 15);
                        ticking = false;
                        Manager.getInstance().setCurrentStat(GameStats.END);
                        Bukkit.getScheduler().cancelTask(taskid);

                    }
                }
                if (Manager.getInstance().isStat(GameStats.END) && ticking) {
                    if (counter <= 10 * 20 && counter > 0) {
                        for (Player p : Manager.getInstance().getPlayers()) {
                            message(p, defaultTranslation().getGame_end_1()+ ChatColor.BLUE + counter / 20 + defaultTranslation().getGame_end_2());
                        }
                    }
                    if (counter == 0) {
                        for(Player p : Bukkit.getOnlinePlayers()){
                            p.kickPlayer("");
                        }
                        Bukkit.shutdown();
                        ticking = false;
                        Bukkit.getScheduler().cancelTask(taskid);
                    }
                }
                counter -= 20;
            }
        }, 0, 20);
    }

    public void start() {
        ticking = true;
    }

    public void stop() {
        ticking = false;
    }

    public void Startup() {
        counter = (20 * 61);
    }

    public void End() {
        counter = (20 * 11);
    }

    public void reset() {
        counter = (20 * 61);
    }

    public boolean started() {
        return ticking;
    }

    public String getTime() {
        //170
        //2
        //
        int s = counter / 20;
        int m = s / 60;
        s = s - m * 60;
        return m + " Minuten und " + s + " Sekunden";
    }

    public void forceStart(){
        counter = (20 * 11);
    }

}
